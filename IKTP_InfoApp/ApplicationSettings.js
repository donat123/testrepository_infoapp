/**
 * Created by Travin on 14.04.2015.
 */
define([], (function(){
    return {
        selectors:{
            mapContainer: '.map',
            delete_link: '.delete-popup-link',
            info_message: '.info-message'
        },
        classes:{
            mapContainerClassName: 'map',
            popover: 'custom-popover',
            delete_link: 'delete-popup-link',
            info_message: 'info-message',
            css_triangle: 'css-triangle'
        },
        eventNames:{
            getPowerSourceAttr: "getPowerSourceAttr"
        }
    }
})())