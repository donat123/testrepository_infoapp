/**
 * Created by Travin on 14.04.2015.
 */
define(['Interfaces/IMapLibInitializer', 'applicationSettings'], function(MapLibInitializer, settings){
    return function(){
        MapLibInitializer.call(this);

        this.init = function(parameters){

            var appSettings = settings;

            var view = new ol.View({
                center: ol.proj.transform([30.216667, 59.90], 'EPSG:4326', 'EPSG:3857'),
                zoom:10
            });

            return new ol.Map({
                layers: [
                    new ol.layer.Tile({
                        source: new ol.source.OSM()
                    })
                ],
                controls: ol.control.defaults(),
                target: appSettings.classes.mapContainerClassName,
                view: view
            });
        };
    }
})