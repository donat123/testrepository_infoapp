/**
 * Created by Travin on 14.04.2015.
 */
define(['Interfaces/IRESTService'], function(interface){
    return function(){
        interface.call(this);

        this.getPowerSourceAttrById = function(powerSourceId){
            var view;
            $.ajax({
                url : "http://trollsmedjan.ru:8080/burst/powersource?id=" + powerSourceId,
                async : false
            }).done(function(data){
                view = data;
            });
            return view;
        };



        this.getData = function(){
            var myData;
            $.ajax({
                url: 'http://trollsmedjan.ru:8080/burst/rest_read_powersources',
                async: false
            }).done(function(data){
                myData = data;
            }).error(function(data){
                console.log(data);
            });

            return myData;
        };
    }
})


