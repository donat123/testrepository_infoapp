/**
 * Created by Travin on 14.04.2015.
 */
define(['Interfaces/IMapLibInitializer', 'Interfaces/IRESTService', 'Interfaces/IMapManager', 'ApplicationSettings'], function(MapInitializer, RESTService, MapManager, settings) {
        return function () {
            var restService = new RESTService();
            var appSettings = settings;


            this.startApp = function(){
                var data = restService.getData();

                var map = (new MapInitializer).init(data);
                (new MapManager).init(map, data.powerSources);

                createBindings();
        };

            function createBindings(){
                $(document).on(appSettings.eventNames.getPowerSourceAttr, function(e, id, calback){
                    var data = restService.getPowerSourceAttrById(id);
                    calback(data);
                })
            }
    }
});