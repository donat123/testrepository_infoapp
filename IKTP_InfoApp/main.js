/**
 * Created by Travin on 14.04.2015.
 */
requirejs.config({
    shim: {
        'ol':{
            deps: [],
            exports: 'ol'
        }
    },
    map: {
        'core': {
            'Interfaces/IMapLibInitializer': 'MapInitializers/OlMapInitializer',
            'Interfaces/IRESTService': 'RESTService',
            'Interfaces/IMapManager': 'MapManagers/OlMapManager'
        },
        'MapManagers/OlMapManager' : {
            'Interfaces/IRESTService': 'RESTService'
        }
    }
});

require(['core'], function(Core){
    (new Core()).startApp();
});