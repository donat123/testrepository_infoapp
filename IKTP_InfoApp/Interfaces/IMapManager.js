/**
 * Created by Travin on 14.04.2015.
 */
define([], function(){
    return function(){
        this.init = function(map, data){throw new Error('You didn\'t implement IMapManager ' +
        'or didn\'t implement init method in your service implimentation');
        };

        this.drawFigures = function(){throw new Error('You didn\'t implement IMapManager ' +
        'or didn\'t implement drawElements method in your service implimentation');};

        this.showSourcePointAttr = function(){throw new Error('You didn\'t implement IMapManager ' +
        'or didn\'t implement showSourcePointAttr method in your service implimentation');};
    }
})