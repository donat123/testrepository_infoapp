/**
 * Created by Travin on 14.04.2015.
 */
define([], function(){
    return function IRESTService(){
        this.getData = function(data){throw new Error('You didn\'t implement IRESTService ' +
        'or didn\'t implement getData method in your service implimentation');};

        this.getPowerSourceAttrById = function(powerSourceId){throw new Error('You didn\'t implement IRESTService ' +
        'or didn\'t implement getPowerSourceAttrById method in your service implimentation');};
    };
})