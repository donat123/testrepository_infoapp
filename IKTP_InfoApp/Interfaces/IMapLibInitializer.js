/**
 * Created by Travin on 14.04.2015.
 */
define([], function(){
    return function(){
        this.init = function(){
            throw new Error('You didn\'t implement IMapLibInitializer ' +
            'or didn\'t implement init method in your service implimentation');
        };
    }
})