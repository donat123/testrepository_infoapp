/**
 * Created by Travin on 14.04.2015.
 */
define(['Interfaces/IMapManager', 'Interfaces/IRESTService', 'applicationSettings'], function(interface, restService, settings){
    return function(){
        interface.call(this);
        var RESTService = new restService();
        var appSettings = settings;
        var $popup,
            popupOverlay;

        this.init = function(map, data){
            _map = map;
            this.drawFigures(data);
        };

        this.showSourcePointAttr = function(powerSource){
            if(powerSource.isPowerSource){
                if(!powerSource.powerSourceAttrView){
                    powerSource.powerSourceAttrView = null;
                }
                loadAttribute(powerSource.id,  powerSource);
                showPowerSourceAttributesBaloon(powerSource);
            }
        };

        this.drawFigures = function(powerSources){
            var modify;
            var sourceAreasCount;
            var that = this;

            var featureOverlay = new ol.FeatureOverlay();
            for(var i = 0; i < powerSources.length; i++){
                featureOverlay.addFeature(getFeature(powerSources[i]));
            }

            featureOverlay.setMap(_map);

            sourceAreasCount = featureOverlay.getFeatures().getArray().length;

            modify = new ol.interaction.Modify({
                features: featureOverlay.getFeatures()
            });
            modify.handleUpEvent_ = function(){};
            modify.handleDragEvent_ = function(){};

            modify.handleDownEvent_ = function(mapBrowserEvent){
                var features = [];
                mapBrowserEvent.map.forEachFeatureAtPixel(mapBrowserEvent.pixel,
                    function (feature, layer) {
                        if(feature.isPowerSource) {
                            features.push(feature);
                        }
                    });

                if(features.length == 1){
                    that.showSourcePointAttr(features[0]);
                }else if(features.length > 1){
                    throw new Error("choose single point, please")
                }
            };

            modify.handleEvent = function(mapBrowserEvent) {
                var handled;
                if (!mapBrowserEvent.map.getView().getHints()[ol.ViewHint.INTERACTING] &&
                    mapBrowserEvent.type == ol.MapBrowserEvent.EventType.POINTERMOVE) {
                    handlePointerMove(mapBrowserEvent);
                }
                if (!goog.isNull(this.vertexFeature_) && this.snappedToVertex_ &&
                    this.deleteCondition_(mapBrowserEvent)) {
                    var geometry = this.vertexFeature_.getGeometry();
                    goog.asserts.assertInstanceof(geometry, ol.geom.Point);
                    handled = this.removeVertex_();
                }
                return ol.interaction.Pointer.handleEvent.call(this, mapBrowserEvent) &&
                    !handled;
            };

            var handlePointerMove = function handlePointerMove(evt) {
                var features = [];
                evt.map.forEachFeatureAtPixel(evt.pixel,
                    function (feature, layer) {
                        if(feature.isPowerSource) {
                            features.push(feature);
                        }
                    });

                if(features.length > 0){
                    for(var i = 0; i < features.length; i++){
                        if(features[i].isPowerSource){
                            if((sourceAreasCount + features.length) >  modify.features_.array_.length){
                                features[i].powerSourceArea.setStyle(features[i].selectStyle);
                                modify.features_.push(features[i].powerSourceArea);
                            }
                        }
                    }
                    return;
                }

                while(sourceAreasCount < modify.features_.array_.length){
                    modify.features_.pop();
                }
            };

            _map.addInteraction(modify);
        };

        function createPopup(){
            $popup = $('<div class="' + appSettings.classes.popover + '"></div>');
            $('<a href="#" class="' + appSettings.classes.delete_link + '"></a>').appendTo($popup);
            $('<div class="' + appSettings.classes.info_message + '"></div>').appendTo($popup);
            $('<div class="' + appSettings.classes.css_triangle + '"></div>').appendTo($popup);
            popupOverlay = new ol.Overlay({
                element: $popup
            });
            $popup.find(appSettings.selectors.delete_link).bind('click', function(e){
                $popup.hide();
            });
        };

        function getFeature(powerSource){
            var point = new ol.Feature(new ol.geom.Point(transformLatLonToMercator(powerSource.location.coordinates)));
            point.isPowerSource = true;
            point.id = powerSource.id;
            point.powerSourceArea = buildPowerSourceArea(powerSource);
            point.selectStyle = buildPowerSourceStyle(powerSource);
            point.setStyle(point.selectStyle);//ol method

            return point;
        };

        function showPowerSourceAttributesBaloon(powerSource){
            if(!popupOverlay){
                createPopup();
                _map.overlays_.push(popupOverlay);
            }
            $popup.show();
            $(appSettings.selectors.info_message).children().remove();
            $(powerSource.powerSourceAttrView).appendTo($(appSettings.selectors.info_message));
            //add text to info_message
            _map.getOverlays().getArray()[0].setPosition(powerSource.getGeometry().getCoordinates());
        };

        function loadAttribute(powerSourceId, powerSource){
            $(document).trigger(appSettings.eventNames.getPowerSourceAttr, [powerSourceId, function(data){powerSource.powerSourceAttrView = data;}])
        };



        function buildPowerSourceStyle(powerSource){
            /*TODO make style from power source server data, when server style model will be*/
            return new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 7,
                    fill: new ol.style.Fill({
                        color: 'yellow'
                    }),
                    stroke: new ol.style.Stroke({
                        width: 1,
                        color: 'green'
                    })
                }),
                stroke: new ol.style.Stroke({
                    width: 1,
                    color: [255, 255, 0, 2]
                }),
                fill: new ol.style.Fill({
                    color: [255, 0, 255, 0.1]
                })
            });
        };

        function buildPowerSourceArea(powerSource){
            var polygonGeoms = [];
            for(var i = 0; i < powerSource.polygon.coordinates.length; i++){
                var polygonRingsCoords=[];
                for(var j = 0; j < powerSource.polygon.coordinates[i].length; j++){
                    polygonRingsCoords.push(transformLatLonToMercator(powerSource.polygon.coordinates[i][j]));
                }
                polygonGeoms.push(polygonRingsCoords);
            }

            return new ol.Feature(new ol.geom.Polygon(polygonGeoms));
        };

        function transformLatLonToMercator(coordinateLikeArray){
            return ol.proj.transform(coordinateLikeArray, 'EPSG:4326', 'EPSG:3857');
        };

        function transformMercatorToLatLon(coordinateLikeArray){
            return ol.proj.transform(coordinateLikeArray, 'EPSG:3857', 'EPSG:4326');
        };
    };
})